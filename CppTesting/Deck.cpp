#include "Deck.h"

#include <algorithm> 
#include <string>
#include <vector>

using namespace std;

const std::string SUIT[SUIT_MAX] = { "Spades", "Hearts", "Diamonds", "Clubs" };
const int RANK[RANK_MAX] = { 2,3,4,5,6,7,8,9,10,11,12,13,1 };
const std::string NAMES[RANK_MAX] = { "Two","Three","Four","Five",
"Six","Seven","Eight","Nine","Ten","Jack","Queen","King","Ace" };

Deck::Deck()
{
	for (int i = 0; i < SUIT_MAX; i++) {
		for (int j = 0; j < RANK_MAX; j++) {
			string suit = SUIT[i];
			int rank = RANK[j];
			string name = NAMES[j];
			Card card(suit, name, rank);
			deck.push_back(card);
			
		}
	}
}


Deck::~Deck()
{
}

Card Deck::drawCard() {
	Card card = deck.back();
	deck.pop_back();
	return card;
}
void Deck::shuffleDeck() {

	std::random_shuffle(deck.begin(), deck.end());
}

void addToDeck(Card newCard) {

}
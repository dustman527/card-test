#pragma once
#ifndef CARD_H_
#define CARD_H_

#include <string>

using namespace std;

class Card
{
private:
	string cardSuit;
	string cardName;
	int cardValue;
public:
	Card();
	Card(string, string, int);

	string getSuit();
	string getName();
	int getValue();
	string getFullData();

	//destructor
	~Card();

	//mutator methods
	void setSuit(string);
	void setName(string);
	void setValue(int);

};

#endif
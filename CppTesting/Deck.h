#pragma once
#ifndef DECK_H_
#define DECK_H_
#include "Card.h"
#include <string>
#include <vector>

using namespace std;

const int SUIT_MAX(4);
const int RANK_MAX(13);

class Deck
{
private:
	std::vector<Card> deck;
	int maxCardID;
	
public:
	Deck();

	vector<Card> getDeck();
	int getDeckSize();
	Card getCard(string, int);
	Card drawCard();

	~Deck();

	void addToDeck(Card);
	void removeFromDeck(int);
	void shuffleDeck();
};


#endif
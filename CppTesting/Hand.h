#pragma once
#ifndef HAND_H_
#define HAND_H_

#include "Card.h"
#include "Hand.h"
#include <string>
#include <vector>

using namespace std;

const int HANDSIZE_MAX(5);

class Hand
{
private:
	vector<Card> hand;


public:
	Hand();
	int getMaxHandsize();
	vector<Card> getHand();
	int getHandsize();
	Card checkCard(int);
	Card useCard(int);

	~Hand();

	void addToHand(Card);
	void discardRandom();
};


#endif

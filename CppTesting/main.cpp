/*
Testing Program
Dustin Bridgewater
*/

#include <iostream>
#include <iomanip>
#include "Card.h"
#include "Hand.h"
#include "Deck.h"

using namespace std;

void printCard(Card);
void createDeck();



int main()
{
	string s;
	cout << "Creating deck." << endl;
	Deck deck;
	Hand hand; 
	cout << "Shuffling...." << endl;
	deck.shuffleDeck();
	cout << "Done shuffling! Type 'draw' to draw a card. Anything else will exit." << endl;
	while (true) {
		cin >> s;
		if (s == "draw") {
			if (hand.getHandsize() == hand.getMaxHandsize()) {
				cout << "Hand full, cannot draw!" << endl;
				continue;
			}
			Card drawnCard = deck.drawCard();
			hand.addToHand(drawnCard);
			cout << "Card: " << drawnCard.getName() << " of " << drawnCard.getSuit() << endl;
			continue;
		}
		else if (s == "use") {
			if (hand.getHandsize() < 1) {
				cout << "Hand empty!" << endl;
				continue;
			}
			int use;
			cout << "Which card would you like to play?" << endl;
			cin >> use;
			if (cin.fail()) {
				continue;
			}
			else {
				if (use < hand.getHandsize()) {
					Card usedCard = hand.useCard(use);

					cout << "Card played: " << usedCard.getName() << " of " << usedCard.getSuit() << endl;
					continue;
				}
				else {
					cout << "Card not found." << endl;
					continue;
				}
			}
		}
		else if (s == "look") {
			if (hand.getHandsize() < 1) {
				cout << "Hand empty!" << endl;
				continue;
			}
			int use;
			cout << "Which card would you like to check?" << endl;
			cin >> use;
			if (cin.fail()) {
				continue;
			}
			else {
				if (use < hand.getHandsize()) {
					Card usedCard = hand.checkCard(use);

					cout << "Card details: " << usedCard.getName() << " of " << usedCard.getSuit() << endl;
					continue;
				}
				else {
					cout << "Card not found." << endl;
					continue;
				}
			}
		}
		else if (s == "check") {
			if (hand.getHandsize() < 1) {
				cout << "Hand empty!" << endl;
				continue;
			}
			int handSize = hand.getHandsize();
			for(int i = 0; i < handSize; i++) {
				Card currentCard = hand.checkCard(i);
				cout << "Card #" << i << " - " << currentCard.getName() << " of " << currentCard.getSuit() << endl;
			}
			continue;
		}
		else {
			// the number was correct, so make your magic happen and then...
			break;
		}
	}
}

void createDeck() {

}
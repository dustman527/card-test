#include "Card.h"
#include <iostream>
using namespace std;


Card::Card()
{
	cardSuit = "Spades";
	cardName = "Ace";
	cardValue = 1;
}

Card::Card(string suit, string name, int value)
{

	cardSuit = suit;
	cardName = name;
	cardValue = value;
}

Card::~Card()
{
}
//accessors
string Card::getSuit() {
	return cardSuit;
}
string Card::getName() {
	return cardName;
}
int Card::getValue() {
	return cardValue;
}

//mutators
void Card::setSuit(string suit) {
	cardSuit = suit;
}
void Card::setName(string name) {
	cardName = name;
}
void Card::setValue(int value) {
	cardValue = value;
}
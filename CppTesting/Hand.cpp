#include "Hand.h"
#include "Deck.h"
#include <iostream>
#include <random>
#include <algorithm>
using namespace std;



Hand::Hand()
{
}


Hand::~Hand()
{
}

vector<Card> Hand::getHand() {
	return hand;
}
int Hand::getMaxHandsize() {
	return HANDSIZE_MAX;
}
int Hand::getHandsize() {
	return hand.size();
}
Card Hand::checkCard(int position) {
	Card checkedCard = hand.at(position);

	return checkedCard;
}

Card Hand::useCard(int position) {
	Card usedCard = hand.at(position);
	if(position > 0 )
	hand.erase(hand.begin() + (position - 1));
	else
		hand.erase(hand.begin());



	return usedCard;
}

void Hand::addToHand(Card card) {
	hand.push_back(card);
}

void Hand::discardRandom() {
	int position;
	int lowest = 0, highest = hand.size();
	int range = (highest - lowest) + 1;
	position = lowest + int(range*rand() / (RAND_MAX + 1.0));

	Card discardedCard = hand.at(position);
	hand.erase(hand.begin() + (position - 1 ));


}